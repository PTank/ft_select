# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: akazian <akazian@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 16:00:35 by akazian           #+#    #+#              #
#    Updated: 2014/01/12 21:12:16 by akazian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			=	ft_select
CC				=	gcc
CFLAGS			=	-Wall -Wextra -Werror -o3
LIBFTDIR		=	./libft/
LIBFTH			=	-I$(LIBFTDIR)
LIBFTFLAGS		=	-L$(LIBFTDIR) -lft -ltermcap
INCS_DIR		=	includes
OBJS_DIR		=	objects
SRCS_DIR		=	sources
SRCS			=	main.c\
				ft_error.c\
				xtgetstr.c\
				init_xtget.c\
				op_term.c\
				ft_output_c.c\
				ft_cirlst.c\
				ft_read.c\
				ft_exit.c\
				ft_print_arg.c\
				ft_putstrn_fd.c\
				ft_key.c\
				ft_putsnb.c\
				ft_signal.c\
				ft_color.c

OBJS 			=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))

all				:	$(NAME)

$(NAME)			:	$(OBJS_DIR) $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LIBFTFLAGS)

$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCS_DIR) $(LIBFTH)

$(OBJS_DIR)	:	makelibft
	mkdir -p $(OBJS_DIR)

makelibft:
	make -C $(LIBFTDIR)

.PHONY: clean all re fclean

fclean			:	clean
	rm -f $(NAME)

clean			:
	rm -rf $(OBJS_DIR)

re				:	fclean all
