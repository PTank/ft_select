/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 12:14:11 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 20:58:37 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _FT_SELECT_H
# define _FT_SELECT_H

# include <stdlib.h>
# include <termios.h>
# include <term.h>
# include <termcap.h>
# include <curses.h>
# include <sys/ioctl.h>
# include <unistd.h>
# include <signal.h>
# include <libft.h>

# define TRUE 1
# define FALSE 0
# define UP 4283163
# define DOWN 4348699
# define RIGHT 4414235
# define LEFT 4479771
# define ESC 27
# define SPACE 32
# define BACKSPACE 2117294875
# define DEL 127
# define RETURN 10

typedef struct	s_xtget
{
	char		*clean;
	char		*sostr;
	char		*sestr;
	char		*cmstr;
	char		*unstr;
	char		*unest;
	char		*cuvi;
	char		*cuve;
	char		*cuti;
	char		*cute;
	char		*area;
}				t_xtget;

typedef struct		s_cirlst
{
	struct s_cirlst	*next;
	struct s_cirlst	*back;
	char			*name;
	int				select;
	int				curser;
}					t_cirlst;

typedef struct		s_env
{
	t_cirlst		**b;
	t_xtget			**xg;
	struct termios	**term;
	struct termios	**save_term;
	int				x;
	int				y;
	int				fd;
	int				enter;
	int				arg;
	int				select;
}					t_env;

# define FD e->fd

void	ft_putstrn_fd(char *s, int fd, int size);
int		ft_error(char *info, char *file);
char	*xtgetstr(char *id, char **area);
int		output_c(int c);
void	init_xtget(t_xtget **xg, t_env *e);
int		open_term(struct termios **term, t_env *e, struct termios **save);
int		ft_read(int argc, t_env *e);
void	ft_exit(t_env *e);
void	ft_print_arg(int argc, t_env *e);
void	ft_return(t_env *e);
int		ft_putsnb(t_env *e, int value, char *str, int x);
void	ft_signal(t_env *e);
void	color_bis(t_cirlst *tmp, int i, t_env *e);
t_env	*s_e(t_env *e);
void	print_cur(t_env *e, t_cirlst **tmp);

/*
** circular list
*/

t_cirlst	*new_clst_elem(void);
void		add_lelem(t_cirlst **b, t_cirlst *next, t_cirlst *back, char *name);
void		del_clst_elem(t_cirlst **b, t_env *e);
void		del_cirlst(t_cirlst **b, int enter);
void		create_cirlst(t_cirlst **b, char **argv, t_env *e);

#endif
