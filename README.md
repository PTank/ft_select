 _-------------------------------------------------------------------------------------------  
 "THE BEER-WARE LICENSE" (Revision 42):  
 <akazian@student.42.fr> wrote this file. As long as you retain this notice you  
 can do whatever you want with this stuff. If we meet some day, and you think  
 this stuff is worth it, you can buy me a beer in return.  
 --------------------------------------------------------------------------------------------_

#FT_SELECT

##Table of Contents

* [installation](#markdown-header-installation)
	* [Lib](#markdown-header-lib)
	* [Install](#markdown-header-install)
* [Man](#markdown-header-man)
	* [User's guid](#markdown-header-user's-guid)
	* [Command](#markdown-header-command)
	* [explain](#markdown-header-explain)
* [TODO](#markdown-header-todo)

------------
#[INSTALLATION](#markdown-header-table-of-contents)
------------

##[Lib](#markdown-header-table-of-contents)

Use curse and termcap,for unix without obsolet lib add:

    $> sudo apt-get install libncurses5-dev libncursesw5-dev

##[Install](#markdown-header-table-of-contents)

    $> make

---
#[Man](#markdown-header-table-of-contents)
---

##[User's guid](#markdown-header-table-of-contents)

    $> ft_select arg1 arg2 arg3 arg4 etc
 -    select with up/down and space
 -    Enter make return select value

##[Command](#markdown-header-table-of-contents)

    $> Up: move curser up
    $> Down: move curser down
    $> space: select or deselect
    $> q or esc: quit without selection
    $> enter: return select value
	$> del or backspace: del a file off list

##[Explain](#markdown-header-table-of-contents)

###Get terminal

I use the old term lib curse and termcap.

1. I take term name with getenv TERM
2. tgetent term
3. take term size with iotcl
4. tcgetattr take and tcsetattr fix term param

###Circular list

A simple 'ring buffer', with true value for curser and select.
If I up or down more than the term can print head list change.

###Print

Use tgetstr and he's various id to make clean, move curser,
or video mode etc.

----
#[TODO](#markdown-header-table-of-contents)
----

#[TODO](#markdown-header-table-of-contents)

 - add C z.
 - for better scroll, print only when is necessary

[Up](#markdown-header-ft_select)
