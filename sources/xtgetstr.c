/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xtgetstr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 12:14:43 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 21:05:40 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

char	*xtgetstr(char *id, char **area)
{
	char	*cap;

	if (!(cap = tgetstr(id, area)))
	{
		ft_error("tgetstr error", id);
		exit(1);
	}
	return (cap);
}
