/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/08 18:07:05 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 21:15:47 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

static void	ft_space(t_env *e)
{
	t_cirlst	*tmp;

	tmp = *e->b;
	while (tmp->curser != 1)
		tmp = tmp->next;
	if (tmp->select == FALSE)
	{
		tmp->select = TRUE;
		e->select += 1;
	}
	else
	{
		tmp->select = FALSE;
		e->select -= 1;
	}
}

static void	ft_key_up(t_env *e)
{
	t_cirlst	*tmp;

	tmp = *e->b;
	while (tmp->curser != 1)
		tmp = tmp->next;
	tmp->curser = FALSE;
	tmp->back->curser = TRUE;
	if (*e->b == tmp)
		*e->b = tmp->back;
}

static void	ft_key_down(t_env *e)
{
	t_cirlst	*tmp;
	int			i;

	i = 1;
	tmp = *e->b;
	while (tmp->curser != 1)
	{
		tmp = tmp->next;
		i++;
	}
	tmp->curser = FALSE;
	tmp->next->curser = TRUE;
	if (i == ((e->x / 20) * (e->y - 4)) - 1)
		*e->b = (*e->b)->next;
}

int			ft_read(int argc, t_env *e)
{
	char	buff[5] = {0};
	int		key;

	ft_bzero(buff, 5);
	read(FD, buff, 4);
	key = *(unsigned int *) buff;
	if (key == 'q' || key == ESC)
		ft_exit(e);
	if (key == UP && argc != 1)
		ft_key_up(e);
	if (key == DOWN && argc != 1)
		ft_key_down(e);
	if (key == SPACE)
		ft_space(e);
	if (key == DEL || key == BACKSPACE)
		if (*e->b)
			del_clst_elem(e->b, e);
	if (key == RETURN)
		ft_return(e);
	return (0);
}
