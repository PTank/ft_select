/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/08 18:19:17 by akazian           #+#    #+#             */
/*   Updated: 2014/01/13 15:36:55 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

void	ft_exit(t_env *e)
{
	tputs((*e->xg)->sestr, 1, output_c);
	tputs((*e->xg)->cuve, 1,  output_c);
	tputs((*e->xg)->clean, 1, output_c);
	tputs((*e->xg)->cute, 1, output_c);
	tcsetattr(STDIN_FILENO, TCSANOW, (*e->save_term));
	free(*e->term);
	free(*e->save_term);
	if (*e->b)
	{
		del_cirlst(e->b, e->enter);
		ft_putchar('\n');
	}
	else
		ft_error("No more arg", NULL);
	exit(0);
}
