/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_term.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 15:54:36 by akazian           #+#    #+#             */
/*   Updated: 2014/01/13 15:36:16 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>
#include <fcntl.h>

static int	open_tty(t_env *e)
{
	int	fd;
	struct winsize	size;

	fd = 0;
	if (ioctl(STDIN_FILENO, TIOCGWINSZ, (char*) &size) < 0
			&& ft_error("Error", "TIOCGWINSZ"))
		return (1);
	e->y = size.ws_row;
	e->x = size.ws_col;
	fd = open(ttyname(STDIN_FILENO), O_RDWR);
	if (fd < 0)
	{
		ft_error("No fd", NULL);
		ft_exit(e);
	}
	FD = fd;
	return (0);
}

static int	env_var(t_env *e)
{
	int	ret;

	ret = open_tty(e);
	e->enter = FALSE;
	return (ret);
}

int			open_term(struct termios **term, t_env *e, struct termios **save)
{
	char			bp[1024];
	char			*ter;

	if (!(ter = getenv("TERM")) && ft_error("Can't find term", "getenv"))
		return (1);
	if (tgetent(bp, ter) != 1 && ft_error("Can't find term", ter))
		return (1);
	if (isatty(STDIN_FILENO) == 0 && ft_error("Is not a term", NULL))
		return (1);
	if (env_var(e))
		return (1);
	*term = malloc(sizeof(struct termios));
	*save = malloc(sizeof(struct termios));
	tcgetattr(STDIN_FILENO, *save);
	tcgetattr(STDIN_FILENO, *term);
	(*term)->c_lflag &= ~(ICANON | ECHO);
	(*term)->c_cc[VMIN] = 1;
	(*term)->c_cc[VTIME] = 0;
	tcsetattr(STDIN_FILENO, TCSAFLUSH, *term);
	e->save_term = save;
	e->term = term;
	return (0);
}
