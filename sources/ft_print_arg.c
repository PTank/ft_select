/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_arg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/09 14:11:23 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 21:13:49 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

static void	put_title(t_env *e)
{
	int	x;

	tputs((*e->xg)->clean, 1, output_c);
	tputs(tgoto((*e->xg)->cmstr, 0, 0), 1, output_c);
	ft_putstr_fd("\033[42m FT_SELECT ", FD);
	x = 11;
	while (x < e->x)
	{
		tputs(tgoto((*e->xg)->cmstr, x, 0), 1, output_c);
		if ((x += 2) < e->x)
			ft_putstr_fd("\033[43m  ", FD);
	}
	if (e->select < 1000000 && (x = 11))
	{
		tputs(tgoto((*e->xg)->cmstr, x, 0), 1, output_c);
		if (x == 11 && x + 16 < e->x)
			x += ft_putsnb(e, e->select, " selected:", 16);
	}
	if (e->arg < 1000000)
	{
		tputs(tgoto((*e->xg)->cmstr, x, 0), 1, output_c);
		if (x == 27 && x + 18 < e->x)
			x += ft_putsnb(e, e->arg, " total list:", 18);
	}
	ft_putstr_fd("\033[0m", FD);
}

static void	put_help(t_env *e, int argc)
{
	int	x;

	x = 0;
	tputs(tgoto((*e->xg)->cmstr, 0, e->y), 1, output_c);
	ft_putstr_fd("\033[44m help: ",FD);
	x += 7;
	while (x < e->x)
	{
		tputs(tgoto((*e->xg)->cmstr, x, e->y), 1, output_c);
		if (x == 7 && (x += 14) < e->x)
			ft_putstr_fd("\033[44m space: select", FD);
		if (x == 21 && (x += 32) < e->x)
			ft_putstr_fd("\033[44m, del/bacspace: sup file on list", FD);
		if (x == 53 && (x += 13) < e->x)
			ft_putstr_fd("\033[44m, esc/q: quit", FD);
		if (x == 66 && (x += 22) < e->x)
			ft_putstr_fd("\033[44m, enter: return select", FD);
		else if (x && (x += 2) < e->x)
			ft_putstr_fd("\033[44m  ", FD);
	}
	ft_putstr_fd("\033[0m", FD);
	tputs(tgoto((*e->xg)->cmstr, x, 0), 1, output_c);
	ft_read(argc, e);
}

static int	color_lst(t_cirlst *tmp, t_env *e)
{
	int	i;

	i = ft_strlen(tmp->name) - 1;
	if (tmp->name[i - 1] == '.')
	{
		if (tmp->name[i] == 'c')
			ft_putstr_fd("\033[31m", FD);
		else if (tmp->name[i] == 'o')
			ft_putstr_fd("\033[35m", FD);
		else if (tmp->name[i] == 'a')
			ft_putstr_fd("\033[36m", FD);
		else if (tmp->name[i] == 'h')
			ft_putstr_fd("\033[33m", FD);
	}
	else if (tmp->name[i - 2] == '.')
		color_bis(tmp, i, e);
	ft_putstrn_fd(tmp->name, FD, 19);
	ft_putendl_fd("\033[0m", FD);
	tputs((*e->xg)->sestr, 1, output_c);
	tputs((*e->xg)->unest, 1, output_c);
	return (1);
}

static int	print_y(t_env *e, int argc, t_cirlst **tmp, int x)
{
	int				y;
	int				s;

	y = 1;
	s = 0;
	while (y++ <= (e->y - 4) && argc > 1)
	{
		tputs(tgoto((*e->xg)->cmstr, x, y), 1, output_c);
		if (*tmp)
		{
			print_cur(e, tmp);
			if ((*tmp)->select == TRUE)
				tputs(tgoto((*e->xg)->sostr, x, y), 1, output_c);
			if ((e->x / 20 - 1) == x / 20 && y == e->y - 3 && (s = 1))
			{
				ft_putendl_fd("(...)", FD);
				break ;
			}
			if (color_lst(*tmp, e) && (*e->b)->back != *tmp)
				*tmp = (*tmp)->next;
			else if ((s = 1))
				break ;
		}
	}
	return (s);
}

void		ft_print_arg(int argc, t_env *e)
{
	t_cirlst		*tmp;
	int				x;
	int				s;

	x = 0;
	s = 0;
	tmp = *e->b;
	put_title(e);
	while ((x / 20) != (e->x / 20))
	{
		if ((*e->b) == NULL)
			ft_exit(e);
		s = print_y(e, argc, &tmp, x);
		if (s)
			break ;
		x += 20;
	}
	put_help(e, argc);
}
