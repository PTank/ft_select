/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cirlst.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 16:23:26 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 20:47:51 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

t_cirlst	*new_clst_elem(void)
{
	t_cirlst	*new;

	if (!(new = ft_memalloc(sizeof(t_cirlst))))
		return (NULL);
	new->next = NULL;
	new->back = NULL;
	new->name = NULL;
	new->select = FALSE;
	new->curser = FALSE;
	return (new);
}

void		add_lelem(t_cirlst **b, t_cirlst *next, t_cirlst *back, char *name)
{
	t_cirlst	*tmp;

	if (!*b)
		*b = new_clst_elem();
	tmp = *b;
	tmp->next = next;
	tmp->back = back;
	tmp->name = name;
}

void		del_clst_elem(t_cirlst **b, t_env *e)
{
	t_cirlst	*tmp;

	tmp = *b;
	if ((*b) == (*b)->next)
	{
		free(*b);
		(*b) = NULL;
		return ;
	}
	while (tmp->curser != TRUE)
		tmp = tmp->next;
	if (*b == tmp)
		*b = tmp->next;
	if (tmp->back && tmp->next)
	{
		tmp->back->next = tmp->next;
		tmp->next->back = tmp->back;
		tmp->next->curser = TRUE;
		if (tmp->select == TRUE)
			e->select -= 1;
	}
	free(tmp);
	tmp = NULL;
	e->arg -= 1;
}

void		del_cirlst(t_cirlst **b, int enter)
{
	t_cirlst	*tmp;
	t_cirlst	*next;
	int			i;

	i = 0;
	tmp = *b;
	while (tmp)
	{
		if (enter && tmp->select)
		{
			if (i != 0)
				ft_putchar(' ');
			ft_putstr(tmp->name);
			i = 1;
		}
		if (tmp->next == tmp)
		{
			free(tmp);
			break ;
		}
		next = tmp->next;
		tmp->back->next = NULL;
		free(tmp);
		tmp = next;
	}
}

void		create_cirlst(t_cirlst **b, char **argv, t_env *e)
{
	t_cirlst	*tmp;
	t_cirlst	*head;

	tmp = NULL;
	head = NULL;
	while (*argv)
	{
		if (!tmp)
		{
			add_lelem(&tmp, NULL, NULL, *argv);
			head = tmp;
		}
		else
		{
			add_lelem(&tmp->next, NULL, tmp, *argv);
			tmp = tmp->next;
			tmp->back->next = tmp;
		}
		argv++;
	}
	if (tmp && (tmp->next = head) && (head->back = tmp))
		head->curser = TRUE;
	*b = head;
	e->b = b;
}
