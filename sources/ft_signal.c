/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/11 17:24:35 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 21:16:30 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

t_env		*s_e(t_env *e)
{
	static t_env	*env = NULL;

	if (env == NULL)
		env = e;
	return (env);
}

void		si_q(int snum)
{
	t_env	*e;

	(void)snum;
	e = s_e(NULL);
	ft_exit(e);
}

void		si_z(int snum)
{
	t_env			*e;
	struct winsize	size;

	e = s_e(NULL);
	if (ioctl(STDIN_FILENO, TIOCGWINSZ, (char*) &size) < 0
			&& ft_error("Error", "TIOCGWINSZ"))
		ft_exit(e) ;
	e->y = size.ws_row;
	e->x = size.ws_col;
	ioctl(0, TIOCSTI, *e->term);
	(void)snum;
}

void		ft_signal(t_env *e)
{
	s_e(e);
	signal(SIGINT, si_q);
	signal(SIGTTIN, si_q);
	signal(SIGTTOU, si_q);
	signal(SIGWINCH, si_z);
}
