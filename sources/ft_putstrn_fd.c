/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstrn_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 16:42:55 by akazian           #+#    #+#             */
/*   Updated: 2014/01/10 16:48:41 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

void	ft_putstrn_fd(char *s, int fd, int size)
{
	size_t	i;

	i = ft_strlen(s);
	if (i < (size_t) size)
		size = i;
	write(fd, s, size);
}
