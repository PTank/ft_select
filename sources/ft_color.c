/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/12 17:28:39 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 17:44:05 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

void	color_bis(t_cirlst *tmp, int i, t_env *e)
{
	if (tmp->name[i - 1] == 's' && tmp->name[i] == 'h')
		ft_putstr_fd("\033[32m", FD);
	if (tmp->name[i - 1] == 'p' && tmp->name[i] == 'y')
		ft_putstr_fd("\033[34m", FD);
	if (tmp->name[i - 1] == 'r' && tmp->name[i] == 'b')
		ft_putstr_fd("\033[35m", FD);
}
