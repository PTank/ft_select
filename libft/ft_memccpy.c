/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 13:44:26 by akazian           #+#    #+#             */
/*   Updated: 2013/12/06 19:15:46 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	char			*dst;
	char			*src;
	unsigned char	c_cp;
	int				end;

	dst = (char *)s1;
	src = (char *)s2;
	c_cp = (unsigned char)c;
	end = 0;
	while (*src != c_cp && n)
	{
		*dst++ = *src++;
		n--;
	}
	if (*src == c_cp && n)
	{
		*dst++ = *src++;
		end++;
	}
	if (!n && !end)
		return (NULL);
	return (dst);
}
