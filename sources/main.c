/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 11:58:10 by akazian           #+#    #+#             */
/*   Updated: 2014/01/15 15:00:05 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** ----------------------------------------------------------------------------
** "THE BEER-WARE LICENSE" (Revision 42):
** <akazian@student.42.fr> wrote this file. As long as you retain this notice you
** can do whatever you want with this stuff. If we meet some day, and you think
** this stuff is worth it, you can buy me a beer in return.
** ----------------------------------------------------------------------------
*/

#include <ft_select.h>

void	print_cur(t_env *e, t_cirlst **tmp)
{
	if ((*tmp)->curser == TRUE)
		tputs((*e->xg)->unstr, 1, output_c);
}

int		main(int argc, char **argv)
{
	t_xtget			*xg;
	t_cirlst		*b;
	struct termios	*term;
	struct termios	*save_term;
	t_env			e;

	if (argc < 2)
	{
		ft_error("arg1 arg2 arg3 arg4", "Usage");
		return (1);
	}
	e.arg = argc - 1;
	e.select = 0;
	create_cirlst(&b, argv + 1, &e);
	if (open_term(&term, &e, &save_term))
		return (1);
	init_xtget(&xg, &e);
	tputs(xg->cuvi, 1, output_c);
	tputs(xg->cuti, 1, output_c);
	while (42)
	{
		ft_signal(&e);
		ft_print_arg(argc, &e);
	}
	return (0);
}
