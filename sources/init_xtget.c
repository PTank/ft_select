/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_xtget.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 13:31:27 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 21:06:10 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

/*
** clean: clean screen 
** sostr && sestr: video mod
** unstr && unest: underline
** cmstr: curser motion
** cuvi && cuve: curser visble mode 
** cuti && cute: cup
*/

void	init_xtget(t_xtget **xg, t_env *e)
{
	char	t[4096];

	*xg = ft_memalloc(sizeof(t_xtget));
	if (!xg)
		return ;
	(*xg)->area = t;
	(*xg)->clean = xtgetstr("cl", &(*xg)->area);
	(*xg)->sostr = xtgetstr("so", &(*xg)->area);
	(*xg)->sestr = xtgetstr("se", &(*xg)->area);
	(*xg)->cmstr = xtgetstr("cm", &(*xg)->area);
	(*xg)->unstr = xtgetstr("us", &(*xg)->area);
	(*xg)->unest = xtgetstr("ue", &(*xg)->area);
	(*xg)->cuvi = xtgetstr("vi", &(*xg)->area);
	(*xg)->cuve = xtgetstr("ve", &(*xg)->area);
	(*xg)->cuti = xtgetstr("ti", &(*xg)->area);
	(*xg)->cute = xtgetstr("te", &(*xg)->area);
	e->xg = xg;
}
