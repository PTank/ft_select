/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putsnb.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/11 15:16:16 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 21:01:31 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

int	ft_putsnb(t_env *e, int value, char *str, int x)
{
	ft_putstr_fd("\033[43m\033[30m", FD);
	ft_putstr_fd(str, FD);
	ft_putnbr_fd(value, FD);
	return (x);
}
